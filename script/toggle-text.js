// $('.checkbox').change(function(){
//   var textSize = this.checked ? '22px' : '16px';
//   var textLineH = this.checked ? '34px' : '26px';
//   var povecColor = this.checked ? 'rgba(53, 105, 178, 1)' : 'rgba(53, 105, 178, 0.4)'
//   var titleSize = this.checked ? '54px' : '36px';
//   var titleLineH = this.checked ? '64px' : '70px';
//   var subtitleSize = this.checked ? '30px' : '20px';
//   var subtitleLineH = this.checked ? '40px' : '30px';
//
//   $('.tab__title.title-modal').css('font-size', titleSize);
//   $('.tab__title.title-modal').css('line-height', titleLineH);
//   $('.tab__subtitle.subtitle-modal').css('font-size', subtitleSize);
//   $('.tab__subtitle.subtitle-modal').css('line-height', subtitleLineH);
//   $('.tab__content-txt.content-txt-modal').css('font-size', textSize);
//   $('.tab__content-txt.content-txt-modal').css('line-height', textLineH);
//   $('.text-enlarge').css('color', povecColor)
// });

$('#checkbox-1').change(function(){
  var textSize = this.checked ? '22px' : '16px';
  var textLineH = this.checked ? '34px' : '26px';
  var povecColor = this.checked ? 'rgba(53, 105, 178, 1)' : 'rgba(53, 105, 178, 0.4)'
  var titleSize = this.checked ? '54px' : '36px';
  var titleLineH = this.checked ? '64px' : '70px';
  var subtitleSize = this.checked ? '30px' : '20px';
  var subtitleLineH = this.checked ? '40px' : '30px';

  $('#checkbox-1-title').css('font-size', titleSize);
  $('#checkbox-1-title').css('line-height', titleLineH);

  $('#checkbox-1-subtitle').css('font-size', subtitleSize);
  $('#checkbox-1-subtitle').css('line-height', subtitleLineH);

  $('#checkbox-1-txt').css('font-size', textSize);
  $('#checkbox-1-txt').css('line-height', textLineH);

  $('#checkbox-1-pisave').css('color', povecColor)
});


$('#checkbox-2').change(function(){
  var textSize = this.checked ? '22px' : '16px';
  var textLineH = this.checked ? '34px' : '26px';
  var povecColor = this.checked ? 'rgba(53, 105, 178, 1)' : 'rgba(53, 105, 178, 0.4)'
  var titleSize = this.checked ? '54px' : '36px';
  var titleLineH = this.checked ? '64px' : '70px';
  var subtitleSize = this.checked ? '30px' : '20px';
  var subtitleLineH = this.checked ? '40px' : '30px';

  $('#checkbox-2-title').css('font-size', titleSize);
  $('#checkbox-2-title').css('line-height', titleLineH);

  $('#checkbox-2-subtitle').css('font-size', subtitleSize);
  $('#checkbox-2-subtitle').css('line-height', subtitleLineH);

  $('#checkbox-2-txt').css('font-size', textSize);
  $('#checkbox-2-txt').css('line-height', textLineH);

  $('#checkbox-2-pisave').css('color', povecColor)
});


$('#checkbox-3').change(function(){
  var textSize = this.checked ? '22px' : '16px';
  var textLineH = this.checked ? '34px' : '26px';
  var povecColor = this.checked ? 'rgba(53, 105, 178, 1)' : 'rgba(53, 105, 178, 0.4)'
  var titleSize = this.checked ? '54px' : '36px';
  var titleLineH = this.checked ? '64px' : '70px';
  var subtitleSize = this.checked ? '30px' : '20px';
  var subtitleLineH = this.checked ? '40px' : '30px';

  $('#checkbox-3-title').css('font-size', titleSize);
  $('#checkbox-3-title').css('line-height', titleLineH);

  $('#checkbox-3-subtitle').css('font-size', subtitleSize);
  $('#checkbox-3-subtitle').css('line-height', subtitleLineH);

  $('#checkbox-3-txt').css('font-size', textSize);
  $('#checkbox-3-txt').css('line-height', textLineH);

  $('#checkbox-3-pisave').css('color', povecColor)
});


$('#checkbox-4').change(function(){
  var textSize = this.checked ? '22px' : '16px';
  var textLineH = this.checked ? '34px' : '26px';
  var povecColor = this.checked ? 'rgba(53, 105, 178, 1)' : 'rgba(53, 105, 178, 0.4)'
  var titleSize = this.checked ? '54px' : '36px';
  var titleLineH = this.checked ? '64px' : '70px';
  var subtitleSize = this.checked ? '30px' : '20px';
  var subtitleLineH = this.checked ? '40px' : '30px';

  $('#checkbox-4-title').css('font-size', titleSize);
  $('#checkbox-4-title').css('line-height', titleLineH);

  $('#checkbox-4-subtitle').css('font-size', subtitleSize);
  $('#checkbox-4-subtitle').css('line-height', subtitleLineH);

  $('#checkbox-4-txt').css('font-size', textSize);
  $('#checkbox-4-txt').css('line-height', textLineH);

  $('#checkbox-4-pisave').css('color', povecColor)
});


$('#checkbox-5').change(function(){
  var textSize = this.checked ? '22px' : '16px';
  var textLineH = this.checked ? '34px' : '26px';
  var povecColor = this.checked ? 'rgba(53, 105, 178, 1)' : 'rgba(53, 105, 178, 0.4)'
  var titleSize = this.checked ? '54px' : '36px';
  var titleLineH = this.checked ? '64px' : '70px';
  var subtitleSize = this.checked ? '30px' : '20px';
  var subtitleLineH = this.checked ? '40px' : '30px';

  $('#checkbox-5-title').css('font-size', titleSize);
  $('#checkbox-5-title').css('line-height', titleLineH);

  $('#checkbox-5-subtitle').css('font-size', subtitleSize);
  $('#checkbox-5-subtitle').css('line-height', subtitleLineH);

  $('#checkbox-5-txt').css('font-size', textSize);
  $('#checkbox-5-txt').css('line-height', textLineH);

  $('#checkbox-5-pisave').css('color', povecColor)
});
